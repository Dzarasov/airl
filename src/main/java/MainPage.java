import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class MainPage extends PageBase {

	public MainPage(WebDriver driver) {
		super(driver);
		this.driver=driver;
	}
	
	public void toFlightsTab(){
		driver.get("http://www.priceline.com/");
		WebDriverWait waitDriverTillElementPresented = new WebDriverWait(driver, 10);
		waitDriverTillElementPresented.until(ExpectedConditions
				.visibilityOfElementLocated(By
						.cssSelector("a[id='tab-flights']")));
		WebElement flighTabButton = driver.findElement(By.cssSelector("a[id='tab-flights']"));
		flighTabButton.click();
		
	}
	
	public void clickOrCheckFlightOnly(){
		WebDriverWait waitDriverTillElementPresented = new WebDriverWait(driver, 10);
		waitDriverTillElementPresented.until(ExpectedConditions
				.visibilityOfElementLocated(By
						.cssSelector("input[id='vp-options-air-radio-air']")));
		WebElement flightOnlyRadioButton = driver.findElement(By.cssSelector("a[id='tab-flights']"));
		if(!flightOnlyRadioButton.isSelected()){
			flightOnlyRadioButton.click();
		}
	}
	
	public void selectRoundTripAndEnterFlightData(){
		WebDriverWait waitDriverTillElementPresented = new WebDriverWait(driver, 10);
		waitDriverTillElementPresented.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("span[class='ui-button-text']")));
		List<WebElement>listOfElements = driver.findElements(By.cssSelector("span[class='ui-button-text']"));
		for(int i = 0; i<listOfElements.size(); i++){
			//System.out.println(listOfElements.get(i).getText());
			if(listOfElements.get(i).getText().equals("Round-trip")){
				listOfElements.get(i).click();
				break;
			}
			else{
				System.out.println("#########ELEMENT Round-trip not found###############");
			}
		}
		WebElement emptyElementHelper = driver.findElement(By.id("flights-form"));
		
		WebElement originalCity = driver.findElement(By.id("air-orig-0"));
		originalCity.clear();
		originalCity.sendKeys("San Francisco, CA - San Francisco Intl Airport (SFO)");
		emptyElementHelper.click();
		
		
		WebElement destinationCity = driver.findElement(By.id("air-dest-0"));
		destinationCity.clear();
		destinationCity.sendKeys("New York City, NY - All Airports (NYC)");
		emptyElementHelper.click();

		
		WebElement departureDate = driver.findElement(By.id("air-date-0"));
		departureDate.clear();
		departureDate.sendKeys("12/01/2014");
		emptyElementHelper.click();

		
		WebElement arrivingDate = driver.findElement(By.id("air-date-1"));
		arrivingDate.clear();
		arrivingDate.sendKeys("12/15/2014");
		emptyElementHelper.click();
		
	}
	
	
	
	public void selectPasengerAndCabine() throws InterruptedException{
		String parentWindow= driver.getWindowHandle();
		Select dropdown = new Select(driver.findElement(By.id("adult-passengers")));
		dropdown.selectByVisibleText("1 Adult");
		
		Select dropdown2 = new Select(driver.findElement(By.id("child-passengers")));
		dropdown2.selectByVisibleText("0 Children");
		
		Select dropdown3 = new Select(driver.findElement(By.id("air-cabin-class")));
		dropdown3.selectByVisibleText("Economy/Coach");
		
		driver.findElement(By.id("air-btn-submit-retl")).click();
		
		WebDriverWait waitDriverTillElementPresented = new WebDriverWait(driver, 50);
		waitDriverTillElementPresented.until(ExpectedConditions
				.visibilityOfElementLocated(By
						.cssSelector("h1[class='dest-loc']")));
		
		driver.findElement(By.cssSelector("img[alt='select stops']")).click();
		driver.findElement(By.id("1stop")).click();
		driver.findElement(By.xpath("//*[@id=\"Stops_list\"]/div/span/a/img")).click();
		Thread.sleep(7000);
		List<WebElement>els = driver.findElements(By.cssSelector("span[class='airlinename']"));
		//els.get(5).click();
		for(int i = 0; i<els.size(); i++){
			if(els.get(i).getText().equals("Virgin America")){
				System.out.println(els.get(i).getText());
				els.get(i).click();
				break;
			}
		
		}
		driver.switchTo().window(parentWindow);
		Thread.sleep(5000);
		List<WebElement>listWitSpans = driver.findElements(By.cssSelector("span[class='flightnum']"));
		System.out.println("Flight num is "+listWitSpans.get(0).getText());
		
		List<WebElement>listWitSpansPrice = driver.findElements(By.cssSelector("span[class='dollar']"));
		System.out.println("Price is "+listWitSpansPrice.get(0).getText());
		
		
	}
	
	public void takeScreenshot() throws IOException { 
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); 
	String fileName = UUID.randomUUID().toString(); 
	File targetFile = new File("target/screenshots/" + fileName + ".jpg"); 
	FileUtils.copyFile(scrFile, targetFile); }
	

}
