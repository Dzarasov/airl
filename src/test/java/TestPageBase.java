import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;


public class TestPageBase {

	public WebDriver driver;
	@Parameters("browser")
	@BeforeClass
	public void startUpFirst(String browser) throws MalformedURLException{
	
		if(browser.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "chromedriver");//"/Users/mikhaildzarasov/Desktop/chromedriver");
			driver=new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("FF"))
		{
			 driver = new FirefoxDriver();

		}
		driver.manage().window().maximize();

	}
}
