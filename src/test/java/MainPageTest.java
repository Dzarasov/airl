import java.io.IOException;

import org.testng.annotations.Test;
import static org.testng.Assert.*;



public class MainPageTest extends TestPageBase{

	@Test
	public void launchWebSite() throws InterruptedException, IOException{
		MainPage mainPage = new MainPage(driver);
		mainPage.toFlightsTab();
		mainPage.clickOrCheckFlightOnly();
		mainPage.selectRoundTripAndEnterFlightData();
		mainPage.selectPasengerAndCabine();
		mainPage.takeScreenshot();
	}
	
}
